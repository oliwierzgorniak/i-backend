const {
  uploadImage,
  getAllPhotos,
  getOnePhoto,
  deletePhoto,
  addTagToPhoto,
  addTagsToPhoto,
  getPhotoTags,
  addModificationToHistorySimple,
  getImage,
} = require("./fileController");

const { getRequestData, getMimeType } = require("./utils.js");

const router = async (req, res) => {
  if (req.url === "/api/photos" && req.method === "POST") {
    uploadImage(req);

    res.writeHead(200, { "content-type": "text/plain;charset=utf-8" });
    res.end("image added");
  } else if (req.url === "/api/photos" && req.method === "GET") {
    const photos = getAllPhotos();

    res.writeHead(200, { "content-type": "application/json;charset=utf-8" });
    res.end(photos);
  } else if (req.url.match(/\/api\/photos\/([0-9]+)/) && req.method === "GET") {
    const photo = getOnePhoto(req.url);

    res.writeHead(200, { "content-type": "application/json;charset=utf-8" });
    res.end(photo);
  } else if (
    req.url.match(/\/api\/photos\/([0-9]+)/) &&
    req.method === "DELETE"
  ) {
    const result = await deletePhoto(req.url);

    res.writeHead(result.statusCode, {
      "content-type": "text/plain;charset=utf-8",
    });
    res.end(result.toSend);
  } else if (req.url === "/api/photos" && req.method === "PATCH") {
    const info = await getRequestData(req);
    const result = await addModificationToHistorySimple(info);

    res.writeHead(result.statusCode, {
      "content-type": "text/plain;charset=utf-8",
    });
    res.end(result.toSend);
  } else if (req.url === "/api/photos/tags" && req.method === "PATCH") {
    const addingInfo = await getRequestData(req);
    const result = await addTagToPhoto(addingInfo);

    res.writeHead(result.statusCode, {
      "content-type": "text/plain;charset=utf-8",
    });
    res.end(result.toSend);
  } else if (req.url === "/api/photos/tags/mass" && req.method === "PATCH") {
    const addingInfo = await getRequestData(req);
    const result = await addTagsToPhoto(addingInfo);

    res.writeHead(result.statusCode, {
      "content-type": "text/plain;charset=utf-8",
    });
    res.end(result.toSend);
  } else if (
    req.url.match(/\/api\/photos\/tags\/([0-9]+)/) &&
    req.method === "GET"
  ) {
    const result = getPhotoTags(req.url);

    let contentType;
    if (result.toSend === "[" || result[0] === "{") {
      contentType = "application/json";
    } else {
      contentType = "text/plain";
    }

    res.writeHead(result.statusCode, {
      "content-type": `${contentType};charset=utf-8`,
    });
    res.end(result.toSend);
  } else if (
    req.url.match(/\/api\/photos\/uploads\/(.*)/) &&
    req.method === "GET"
  ) {
    const image = await getImage(req.url);

    res.writeHead(200, {
      "content-type": `${getMimeType(req.url)};charset=utf-8`,
    });
    res.end(image);
  }
};

module.exports = router;
