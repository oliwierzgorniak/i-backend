const {
  getAllRawTags,
  getAllTags,
  getTag,
  addTag,
} = require("./tagsController.js");
const { getRequestData } = require("./utils.js");

const router = async (req, res) => {
  if (req.url === "/api/tags" && req.method === "GET") {
    const tags = getAllTags();

    res.writeHead(200, { "content-type": "application/json;charset=utf-8" });
    res.end(tags);
  } else if (req.url === "/api/tags/raw" && req.method === "GET") {
    const rawTags = getAllRawTags();

    res.writeHead(200, { "content-type": "application/json;charset=utf-8" });
    res.end(rawTags);
  } else if (req.url.match(/\/api\/tags\/([0-9]+)/) && req.method === "GET") {
    const tag = getTag(req.url);

    res.writeHead(200, { "content-type": "application/json;charset=utf-8" });
    res.end(tag);
  } else if (req.url === "/api/tags" && req.method === "POST") {
    const data = await getRequestData(req);
    await addTag(data);

    res.writeHead(200, { "content-type": "text/plain;charset=utf-8" });
    res.end("tag added");
  }
};

module.exports = router;
