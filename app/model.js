const { getRandom } = require("./utils");

let tagsRaw = require("./tagsRaw");

let tags = tagsRaw.map((tagName, i) => {
  return { id: i, name: tagName, popularity: getRandom(10000000) };
});

let photos = [];

let users = [];

module.exports = { tagsRaw, tags, photos, users };
