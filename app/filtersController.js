let { photos } = require("./model");
const {
  getLastElementFromUrl,
  getIndex,
  separatePathByExtension,
} = require("./utils");

const sharp = require("sharp");
const { stat } = require("fs/promises");

async function useFilter(photoUrl, info, newImagePath) {
  const type = info.filterType;

  if (type === "rotate") {
    await sharp(photoUrl).rotate(info.rotateValue).toFile(newImagePath);
  } else if (type === "resize") {
    await sharp(photoUrl)
      .resize(info.widthValue, info.heightValue)
      .toFile(newImagePath);
  } else if (type === "negate") {
    await sharp(photoUrl).negate().toFile(newImagePath);
  } else if (type === "grayscale") {
    await sharp(photoUrl).grayscale().toFile(newImagePath);
  } else if (type === "crop") {
    await sharp(photoUrl)
      .extract({
        width: info.widthValue,
        height: info.heightValue,
        left: info.leftValue,
        top: info.topValue,
      })
      .toFile(newImagePath);
  } else if (type === "tint") {
    await sharp(photoUrl)
      .extract({
        r: info.rValue,
        g: info.gValue,
        b: info.bValue,
      })
      .toFile(newImagePath);
  } else if (type === "flip") {
    await sharp(photoUrl).flip().toFile(newImagePath);
  } else if (type === "flop") {
    await sharp(photoUrl).flop().toFile(newImagePath);
  }
}

async function addFilterOperationToHistory(
  photoIndex,
  filterType,
  newImagePath
) {
  const stats = await stat(newImagePath);
  const modificationTime = stats.mtime;

  photos[photoIndex].history.push({
    status: filterType,
    timestamp: modificationTime,
    url: newImagePath,
  });
}

module.exports = {
  getImageMetadata: async (url) => {
    const id = getLastElementFromUrl(url);
    const index = getIndex(id, photos);
    const photoUrl = photos[index].url;

    const meta = await sharp(photoUrl).metadata();
    return JSON.stringify(meta);
  },

  handleUseFilterRequest: async (infoJSON) => {
    const info = await JSON.parse(infoJSON);
    const photoIndex = getIndex(info.photoId, photos);
    const photoUrl = photos[photoIndex].url;

    if (typeof photoIndex === "undefined") {
      console.log("the photo not found");
    }

    const separatedPath = separatePathByExtension(photoUrl);
    const newImagePath = `${separatedPath.pathNoExtension}-${info.filterType}.${separatedPath.extension}`;

    await useFilter(photoUrl, info, newImagePath);
    await addFilterOperationToHistory(
      photoIndex,
      info.filterType,
      newImagePath
    );
  },
};
