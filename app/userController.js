let { users } = require("./model.js");
const { getLastElementFromUrl, getIndexByValue } = require("./utils.js");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config();
const nodemailer = require("nodemailer");

async function registerUser(dataJSON) {
  let data = await JSON.parse(dataJSON);

  if (!data.name || !data.lastName || !data.email || !data.password) {
    return { message: "data missing", statusCode: 400 };
  }

  if (userExists(data)) {
    return { message: "user exists", statusCode: 409 };
  }

  const encryptedPassword = await bcrypt.hash(data.password, 10);
  data.password = encryptedPassword;

  data.confirmed = false;

  users.push(data);

  const token = await createToken(data.email);
  console.log(token);

  sendVerificationLink(data.email, token);

  return { message: "user created", statusCode: 201 };
}

function userExists(user) {
  for (const u of users) {
    if (u.email === user.email) {
      return true;
    }
  }
  return false;
}

function sendVerificationLink(to, token) {
  const transporter = nodemailer.createTransport({
    port: 1025,
  });

  transporter.sendMail({
    from: "user1@example.com",
    to: to,
    subject: "Verification link",
    html: `<a href="http://localhost:3000/api/user/confirm/${token}">Confirmation link</a>`,
  });
}

async function createToken(email) {
  const token = await jwt.sign(
    {
      email: email,
    },
    process.env.KEY, // powinno być w .env
    {
      expiresIn: "1h", // "1m", "1d", "24h"
    }
  );

  return token;
}

///////////////////////////////////////////////////////

async function confirmUser(url) {
  const token = getLastElementFromUrl(url);

  const result = await verifyToken(token);

  if (result !== "error") {
    const userIndex = getIndexByValue("email", result.email, users);

    users[userIndex].verified = true;
    sendConfiramtionInfo(result.email);

    return { statusCode: 200, message: "token authorized" };
  } else {
    return { statusCode: 401, message: "error while verifing token" };
  }
}

async function verifyToken(token) {
  try {
    let decoded = await jwt.verify(token, process.env.KEY);
    return decoded;
  } catch (ex) {
    console.log(ex.message);

    return "error";
  }
}

function sendConfiramtionInfo(to) {
  const transporter = nodemailer.createTransport({
    port: 1025,
  });

  transporter.sendMail({
    from: "user1@example.com",
    to: to,
    subject: "Verification link",
    text: "user confirmed",
  });
}

///////////////////////////////////////////////////////

async function loginUser(dataJSON) {
  const data = await JSON.parse(dataJSON);

  if (!data.email || !data.password) return;

  const userIndex = getIndexByValue("email", data.email, users);
  const password = users[userIndex].password;
  console.log(password);
  console.log(data.password);
  const isProvidedPasswordCorrect = await bcrypt.compare(
    data.password,
    password
  );

  if (!isProvidedPasswordCorrect) return;

  const loginToken = await createToken(data.email);

  return { loginToken: loginToken };
}

module.exports = { registerUser, confirmUser, loginUser };
