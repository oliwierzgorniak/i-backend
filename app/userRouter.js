const { registerUser, confirmUser, loginUser } = require("./userController");
const { getRequestData } = require("./utils.js");

const router = async (req, res) => {
  if (req.url == "/api/user/register" && req.method === "POST") {
    const data = await getRequestData(req);
    const result = await registerUser(data);

    res.writeHead(result.statusCode, {
      "content-type": "text/plain;charset=utf-8",
    });
    res.end(result.message);
  } else if (
    req.url.match(/\/api\/user\/confirm\/(.*)/) &&
    req.method === "GET"
  ) {
    const result = await confirmUser(req.url);

    res.writeHead(result.statusCode, {
      "content-type": "text/plain;charset=utf-8",
    });
    res.end(result.message);
  } else if (req.url == "/api/user/login" && req.method === "POST") {
    const data = await getRequestData(req);
    const result = await loginUser(data);
    console.log(result);

    //   if(req.headers.authorization && req.headers.authorization.startsWith("Bearer")){
    //     // czytam dane z nagłowka
    //     let token = req.headers.authorization.split(" ")[1]
    //     console.log(token)
    //  }

    res.writeHead(200, {
      "content-type": "application/json;charset=utf-8",
      authorization: `Bearer  ${result.loginToken}`,
    });
    res.end(JSON.stringify(result));
  }
};

module.exports = router;
