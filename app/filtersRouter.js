const {
  getImageMetadata,
  handleUseFilterRequest,
} = require("./filtersController");
const { getRequestData } = require("./utils.js");

const router = async (req, res) => {
  if (
    req.url.match(/\/api\/filters\/metadata\/([0-9]+)/) &&
    req.method === "GET"
  ) {
    const imageMetadata = await getImageMetadata(req.url);

    res.writeHead(200, { "content-type": "application/json;charset=utf-8" });
    res.end(imageMetadata);
  } else if (req.url === "/api/filters" && req.method === "PATCH") {
    const info = await getRequestData(req);
    await handleUseFilterRequest(info);

    res.writeHead(200, { "content-type": "plain/text;charset=utf-8" });
    res.end("filter used");
  }
};

module.exports = router;
