async function getRequestData(req) {
  return new Promise((resolve, reject) => {
    try {
      let body = "";

      req.on("data", (part) => {
        body += part.toString();
      });

      req.on("end", () => {
        resolve(body);
      });
    } catch (error) {
      reject(error);
    }
  });
}

function getRandom(end) {
  return Math.floor(Math.random() * (end + 1));
}

function getNewId(arr) {
  if (arr.length === 0) return 0;

  const lastPhoto = arr[arr.length - 1];
  const lastId = lastPhoto.id;

  return +(lastId + 1);
}

function getLastElementFromUrl(url) {
  for (let i = url.length - 1; i >= 0; i--) {
    if (url[i] === "/") {
      return url.substring(i + 1);
    }
  }
}

function getIndex(id, array) {
  for (let i = 0; i < array.length; i++) {
    if (array[i].id == id) return i;
  }
}

function separatePathByExtension(path) {
  for (let i = path.length - 1; i >= 1; i--) {
    if (path[i] === ".") {
      return {
        pathNoExtension: path.substring(0, i),
        extension: path.substring(i + 1),
      };
    }
  }
}

function getMimeType(url) {
  for (let i = url.length - 1; i >= 1; i--) {
    if (url[i] === ".") {
      const ext = url.substring(i + 1);
      if (ext === "jpg" || ext === "jpeg") {
        return "image/jpeg";
      }

      if (ext === "png") {
        return "image/png";
      }

      if (ext === "webp") {
        return "image/webp";
      }

      if (ext === "avif") {
        return "image/avif";
      }
    }
  }
}

function getIndexByValue(key, value, array) {
  for (let i = 0; i < array.length; i++) {
    if (array[i][key] == value) {
      return i;
    }
  }
}

module.exports = {
  getRandom,
  getRequestData,
  getLastElementFromUrl,
  getNewId,
  getIndex,
  separatePathByExtension,
  getMimeType,
  getIndexByValue,
};
