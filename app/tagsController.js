let { tagsRaw, tags } = require("./model");
const { getLastElementFromUrl, getNewId } = require("./utils");

module.exports = {
  getAllRawTags: () => JSON.stringify(tagsRaw),
  getAllTags: () => JSON.stringify(tags),
  getTag: (url) => {
    const id = getLastElementFromUrl(url);

    for (const tag of tags) {
      if (tag.id == id) return JSON.stringify(tag);
    }
  },
  addTag: async (tagJSON) => {
    let tag = await JSON.parse(tagJSON);
    tag.id = getNewId(tags);

    tags.push(tag);
    tagsRaw.push(tag.name);
  },
};
