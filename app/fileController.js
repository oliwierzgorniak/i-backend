const logger = require("tracer").colorConsole();
const formidable = require("formidable");
const { getNewId, getLastElementFromUrl, getIndex } = require("./utils");
let { photos } = require("./model");
const { rm, readFile } = require("fs/promises");
const { join } = require("path");

function getImageData(fields, files) {
  return {
    id: getNewId(photos),
    album: fields.album,
    originalName: files.file.name,
    url: files.file.path,
    lastChange: "original",
    history: [
      {
        status: "original",
        timestamp: files.file.lastModifiedDate,
      },
    ],
    tags: [],
  };
}

module.exports = {
  uploadImage: (req) => {
    // https://www.npmjs.com/package/formidable
    const options = {
      multiples: true,
      uploadDir: "./uploads",
      keepExtensions: true,
      // filter: filter,
    };

    let form = formidable(options);

    form.parse(req, async (err, fields, files) => {
      if (err) {
        logger.error(err);
      }

      // logger.log(JSON.stringify({ fields, files }, null, 2));

      const imageData = getImageData(fields, files);
      photos.push(imageData);
    });
  },

  getAllPhotos: () => JSON.stringify(photos, null, 2),

  getOnePhoto: (url) => {
    const id = getLastElementFromUrl(url);

    const photoIndex = getIndex(id, photos);
    return JSON.stringify(photos[photoIndex]);
  },

  deletePhoto: async (url) => {
    const id = getLastElementFromUrl(url);
    const photoIndex = getIndex(id, photos);

    if (typeof photoIndex === "undefined") {
      return { statusCode: 404, toSend: "the image not found" };
    }

    // delting file
    await rm(photos[photoIndex].url);

    // removing object from array
    photos = [...photos.slice(0, photoIndex), ...photos.slice(photoIndex + 1)];

    return { statusCode: 200, toSend: "the image deleted" };
  },

  addTagToPhoto: async (addingInfoJSON) => {
    const addingInfo = await JSON.parse(addingInfoJSON);
    const photoIndex = getIndex(addingInfo.photoId, photos);

    if (typeof photoIndex === "undefined") {
      return { statusCode: 404, toSend: "the photo not found" };
    }

    photos[photoIndex].tags.push(addingInfo.tag);
    return { statusCode: 200, toSend: "the tag added to the photo" };
  },

  addTagsToPhoto: async (addingInfoJSON) => {
    const addingInfo = await JSON.parse(addingInfoJSON);
    const photoIndex = getIndex(addingInfo.photoId, photos);

    if (typeof photoIndex === "undefined") {
      return { statusCode: 404, toSend: "the photo not found" };
    }

    addingInfo.tags.forEach((tag) => {
      photos[photoIndex]?.tags.push(tag);
    });

    return { statusCode: 200, toSend: "the tags added to the photo" };
  },

  getPhotoTags: (url) => {
    const id = getLastElementFromUrl(url);
    const photoIndex = getIndex(id, photos);

    if (typeof photoIndex === "undefined") {
      return { statusCode: 404, toSend: "the photo not found" };
    }
    const toSend = JSON.stringify({
      id: id,
      tags: photos[photoIndex].tags,
    });

    return { statusCode: 200, toSend: toSend };
  },

  addModificationToHistorySimple: async (infoJSON) => {
    const info = await JSON.parse(infoJSON);

    const photoIndex = getIndex(info.id, photos);

    if (typeof photoIndex === "undefined") {
      return { statusCode: 404, toSend: "the photo not found" };
    }

    photos[photoIndex].history.push({
      status: info.status,
      timestamp: Date.now(),
    });

    return { statusCode: 200, toSend: "done" };
  },

  getImage: async (url) => {
    const urlSplited = url.split("/");

    const path = join(
      urlSplited[urlSplited.length - 2],
      urlSplited[urlSplited.length - 1]
    );

    const image = await readFile(path);
    return image;
  },
};
