const bcrypt = require("bcrypt");

async function test() {
  const encrypted = await bcrypt.hash("password", 10);
  const isPassOk = await bcrypt.compare(
    "password",
    "$2b$10$ITYoeI2d8l/panXsVNIT6.gtIMjRvaVj2.3t.ci078cnmsdqwUYR6"
  );
  console.log(isPassOk);
}

test();
